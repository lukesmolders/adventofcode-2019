﻿using System;


using static System.Console;

/// Solution 1.1
int[] values = {
    148623,147663,67990,108663,62204,140999,123277,52459,143331,71135,76282,69509,72977,120407,62278,136882,131667,146225,112216,108600,127267,149149,72977,149639,101527,70059,124825,69539,141444,64138,71145,68178,134752,79431,126342,134161,135424,95647,54507,104852,100164,118799,57387,93136,133359,144942,89337,60441,131825,93943,98142,108306,55355,115813,83431,125883,101115,107938,103484,61174,123502,73670,91619,136860,93268,149648,105328,53194,115351,130953,85611,71134,141663,106590,56437,147797,98484,60851,121252,66898,56502,103796,86497,121534,70914,122642,53151,131939,108394,128239,103490,122304,113810,141469,79176,108002,91942,84400,101217,116287
};

int total = 0;

foreach (int module in values)
{
    int fuel = module / 3;
    total += (fuel - 2);
}

WriteLine($"Solution for 1.1: {total}");

// Solution 1.2
int total2 = 0;
foreach (int module in values)
{
    int fuel = module;
    while (fuel > 0)
    {
        int temp = (fuel / 3) - 2;
        if (temp > 0)
        {
            total2 += temp;
            fuel = temp;
        }
        else break;
    }
}

WriteLine($"Solution for 1.2: {total2}");

// Solution 2.1
int[] values21 = {
    1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,1,19,5,23,2,10,23,27,2,27,13,31,1,10,31,35,1,35,9,39,2,39,13,43,1,43,5,47,1,47,6,51,2,6,51,55,1,5,55,59,2,9,59,63,2,6,63,67,1,13,67,71,1,9,71,75,2,13,75,79,1,79,10,83,2,83,9,87,1,5,87,91,2,91,6,95,2,13,95,99,1,99,5,103,1,103,2,107,1,107,10,0,99,2,0,14,0
};

int currentposition = 0;
bool stop = false;
banan();
void banan()
{
    switch (values21[currentposition])
    {
        case 1:
            // adds
            var lookfor1 = values21[currentposition + 1];
            var lookfor2 = values21[currentposition + 2];
            var placetoput2 = values21[currentposition + 3];
            var addstotal = (values21[lookfor1]) + (values21[lookfor2]);
            values21[placetoput2] = addstotal;
            break;
        case 2:
            // multiple
            var lookingfor1 = values21[currentposition + 1];
            var lookingfor2 = values21[currentposition + 2];
            var placetoput = values21[currentposition + 3];
            var multipletotal = values21[lookingfor1] * values21[lookingfor2];
            values21[placetoput] = multipletotal;
            break;
        case 99:
         // stop
        stop = true;
        break;
    }

    currentposition += 4;
    if (currentposition < values21.Length && stop == false) 
    {
        banan();
    }
    Console.WriteLine($"Solution for 2.1 is {values21[0]}");
};
